import React, { Component } from 'react';
import { DragDropContext,Droppable, Draggable } from 'react-beautiful-dnd';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import Icon from '@material-ui/core/Icon'
//import FormGroup from '@material-ui/core/FormGroup';
//import FormControlLabel from '@material-ui/core/FormControlLabel';
//import Checkbox from '@material-ui/core/Checkbox';
//import Grid from '@material-ui/core/Grid';
//import Typography from '@material-ui/core/Typography';
//import ListItemAvatar from '@material-ui/core/ListItemAvatar';
//import ListItemIcon from '@material-ui/core/ListItemIcon';
//import Avatar from '@material-ui/core/Avatar';

class TodoList extends Component {
    
    render() { 
        return ( 
          <DragDropContext onDragEnd={this.props.onDragEnd}>
            <Droppable droppableId="droppable">
              {(provided, snapshot) => (
                <div ref={provided.innerRef}>
                  <List>
                    {this.props.data.map((item, index) => (
                      <Draggable key={item.id} draggableId={item.id} index={index}>
                        {(provided, snapshot) => (
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                    
                          >
                            <ListItem key = {item.id}>
                              <Icon {...provided.dragHandleProps}>drag_handle</Icon> 
                              <ListItemText primary = {item.name}/> 
                              <ListItemSecondaryAction >
                                <IconButton aria-label = "Delete" 
                                  onClick = {() => 
                                    {
                                      this.props.handleClick(item.id)
                                    }
                                }>
                                  <Icon>delete</Icon>
                                </IconButton>
                              </ListItemSecondaryAction>
                            </ListItem>
                          </div>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                    </List>
                </div>
              )}
            </Droppable>
          </DragDropContext>
        )
    }
}
 
export default TodoList;

/*<List>
  {this.props.data.map(todo => 
    <ListItem key={todo.id} className="list">
      <Icon>drag_handle</Icon>
      <ListItemText
        primary={todo.name}
      />
      <ListItemSecondaryAction>
        <IconButton className="btn" aria-label="Delete" onClick={ () => {this.props.handleClick(todo.id)}}>
        <Icon>delete</Icon>
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>,
  )}
</List>*/