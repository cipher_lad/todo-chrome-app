import React, { Component } from 'react';
import './App.css';
import Card from '@material-ui/core/Card';
import TodoList from './components/TodoList';
import TodoInput from './components/TodoInput';


// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

class App extends Component {
  state={
    todos : [],
    idCount:0,
  }

  //onDrag
  onDragEnd(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const newTodos = reorder(
      this.state.todos,
      result.source.index,
      result.destination.index
    );

    localStorage.setItem('todos', JSON.stringify(newTodos))
    this.setState({
      todos:newTodos,
    });
  }

  //addTodo
  addTodo(todoName){
    this.setState(prevState=>{
      const newTodos = prevState.todos.slice().concat({
        name: todoName,
        id: prevState.idCount
      })
      const newIdCount = prevState.idCount + 1;

      localStorage.setItem('todos', JSON.stringify(newTodos))
      localStorage.setItem('idCount', newIdCount)
      return {
        todos:newTodos,
        idCount: newIdCount,
        input :  ''
      }
    })
  }

  //deleteTodo
  deleteTodo(todoId){
    this.setState(prevState =>{
      const newTodos=prevState.todos.slice().filter(todo=> todo.id !== todoId)
       //update localstorage 
       localStorage.setItem('todos', JSON.stringify(newTodos))
      return {
        todos:newTodos
      }
    })
  }

  componentDidMount() {
    let todos = [];
    let idCount = 0;

    if (localStorage.hasOwnProperty('todos')) {
      todos = JSON.parse(localStorage.getItem('todos'));
    }

    if (localStorage.hasOwnProperty('idCount')) {
      idCount = localStorage.getItem('idCount');
    }

    this.setState({
      todos,
      idCount,
    })
  }

  render() {
    return (
      <div className='App'>
        <Card className='main-container'>
          <TodoInput 
            handleSubmit={this.addTodo.bind(this)}
          />
          <TodoList 
            data={this.state.todos} 
            handleClick={this.deleteTodo.bind(this)}
            onDragEnd = {this.onDragEnd.bind(this)}
          />
        </Card>
      </div>
    );
  }
}

export default App;
